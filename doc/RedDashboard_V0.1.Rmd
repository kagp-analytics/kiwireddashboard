---
title: "Red DashBoard Draft"
author: "John McCallum"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  flexdashboard::flex_dashboard

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

loadd(trialTable,merged,mergedRedSens)
redvines <- trialTable[Subprogram %like% 'Red',unique(Group_Code)]
```
Page 1
===================================== 


Column 
----------------------------

```{r HCL plot,echo=FALSE,warning=FALSE,message=FALSE,error=FALSE, results='hide',fig.keep='all'}
redChromaDT   <- dcast(merged[ident %in% redvines & 
                                 measure_name %like% 'Chroma|Red' & 
                                 !metadata %like% 'Grader' & no !=0],
                        ident + year(date) + no ~ measure_name, 
                        value.var='val',
                        fun.aggregate=function(X) mean(as.numeric(X),na.rm=T))
redChromaDT[,HCL:=hcl(h=`Chroma-H`,c=`Chroma-C`,l=`Chroma-L`)]

redChromaDT[,plot(`Chroma-H`,`Chroma-C`,cex=0.8,col=HCL,pch=16,main='Red Blocks HCL Colours')]

```






Column 
----------------------------


### Total Red Vines
```{r}
valueBox(trialTable[Subprogram %like% 'Red',.N], icon = "fa-pencil")
```

### Red Vines to Sensory
```{r}

valueBox(merged[ident %in% redvines & measure_name =='Sensory Comments Taste',uniqueN(ident)],icon = './DATA/Kiwifruitred.png')
```

### Vines by Trial
```{r}

mytable <- merge(trialTable[Subprogram %like% 'Red',.N,by=Trial_Name],trialTable[VineStatus %in% c("Removed Psa","Topped Psa","Under Psa Obs"),.(Psa_removals=.N),by=Trial_Name])
knitr::kable(mytable)
```

Page 2
===================================== 


Column
-------------------------------------

### Field Score vs FAST
```{r message=FALSE, warning=FALSE}
loadd(mergedFieldFASTRed)

mergedFieldFASTRed %>% 
    ggplot(aes(x=redFldIntObs,y=`Red FAST-Intesity Inner`)) +
    geom_jitter(width = 0.2,height=0.2) +
    geom_smooth(method='lm') +
  theme_bw() +
  xlab('Breeders Field Score') 
```

Column
-------------------------------------

### Red and Sensory Scores
```{r}
ggplotly(mergedRedSens %>% 
  ggplot(aes(x=`Red FAST-Intesity Inner`,
             y=`Red FAST-Intensity Outer`,
             Group_Code=Group_Code,
             shape=as.factor(ObsYear))) + 
  geom_jitter(aes(color=`Sensory Overall`),
              width = 0.2,height=0.2)  +
  scale_colour_gradientn(colours=rainbow(4,rev = T)) +
  theme_bw() + 
  xlab('Red FAST-Intensity Inner') )

```


Page 3
===================================== 


Column
-------------------------------------

## Reproducibility

<details><summary>Reproducibility receipt</summary>

```{r}
## datetime
Sys.time()

## repository
if(requireNamespace('git2r', quietly = TRUE)) {
  git2r::repository()
} else {
  c(
    system2("git", args = c("log", "--name-status", "-1"), stdout = TRUE),
    system2("git", args = c("remote", "-v"), stdout = TRUE)
  )
}

## session info
sessionInfo()
```

</details>


