### Build Data for Dashboard
library(kakapoaggr)
library(data.table)
library(PFRBreedR)

## Form Trial Structures
trialnames <- scan(here::here('DATA/redtrials.txt'),what='text',sep='\n')
names(trialnames) <- trialnames
kiwitrials <- data.table(EBTrials(crpId = 5,trlTypeId = 8))
##--------------------------------

## Read in rds files per block
RDSPATH <- '/powerplant/workspace/cfljam/RedMarkerGenetics/220.RedDashBoard/DATA/RDS'
rdsfiles <- list.files(RDSPATH,full.names = T)
names(rdsfiles) <- sapply(rdsfiles,function(X) gsub('.rds','',basename(X)))
DT <- data.table::rbindlist(lapply(rdsfiles,readRDS),idcol = 'Block')
DT[,Block:=gsub('_',' ',Block)]


## Get Property list 
kiwiprops <- data.table(EBProperties(5,inclSets = T))
redprops <- kiwiprops[PrpName %like% 'Red|Cull',PrpShortName]
pedprops <- kiwiprops[PrsName=='Pedigree Properties',PrpShortName]

## Read in the Pedigree Item Data for All the Blocks
PIDataRDS  <- '/powerplant/workspace/cfljam/RedMarkerGenetics/220.RedDashBoard/DATA/PIData.rds'
if (file.exists(PIDataRDS)) {
  PIDT <- readRDS(PIDataRDS)
  } else {
  PIDT <-rbindlist(lapply(trialnames,
                        function(X) data.table(EBPIData(5,
                                                        trlName = X,
                                                        PIprpShortName = c('vineStatus','moveToPosition',pedprops)),
                                               check.names = T)),
                 fill = T,idcol = 'Block')
  setnames(PIDT,'Move.to.Position','Group_Code')
  PIDT[,Breeder.Cross.Code:=gsub('')]
  saveRDS(PIDT,PIDataRDS)
  }

### -------Get Crosses and Calculate Coancestry--------
DTRED <- getFASTRed(DT)
## get red PIs and crosses
redpi <- DTRED[`Red FAST-Intesity Inner` >1,unique(ident)]

DTK <- PIDT[Group_Code %in% redpi & PedigreeItem %like% '^K',c('PedigreeItem' ,'Breeder.Cross.Code')]
DTK[,Cross:=gsub('\\.[0-9]{5}$','',PedigreeItem)]
DTCross <- unique(DTK[,.(Breeder.Cross.Code,Cross)])
myped <- EBPedigree(5,PI=DTCross$Cross)
CrossCoan <- coanFn(DTCross$Cross,myped,retWhat = 'coancestry')
MDSfit <- cmdscale(1-CrossCoan,eig=TRUE, k=2)
MDDT <- data.table(MDSfit$points,keep.rownames = T)
setnames(MDDT,names(MDDT),c('Cross','MDS1','MDS2'))
blockredvines <- DTRED[`Red FAST-Intensity Outer` > 2,unique(ident)]
blockredcrosses <- DTK[PedigreeItem %in% PIDT[Group_Code %in% blockredvines,unique(PedigreeItem)],unique(Cross)]
MDDT[,block:=ifelse(Cross %in% blockredcrosses,T,F)]
##ggplotly(merge(DTCross,MDDT) %>% ggplot(aes(x=MDS1,y=MDS2,color=block,Cross=Breeder.Cross.Code)) + geom_point())
## Red by cross
DTCrossRed <- merge(PIDT,DTRED,by.x= "Group_Code" ,by.y="ident" )[,.(N=.N,medInnerRed=median(`Red FAST-Intesity Inner`,na.rm = T),
                                                                     medOuterRed=median(`Red FAST-Intensity Outer`,na.rm = T)),
                                                                  by=Breeder.Cross.Code]
## merge(merge(DTCross,MDDT),DTCrossRed,by='Breeder.Cross.Code')  
